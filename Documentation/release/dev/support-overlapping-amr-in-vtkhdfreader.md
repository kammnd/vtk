## Support Overlapping AMR in vtkHDFReader

VTK now provides Overlapping AMR dataset support in vtkHDFReader.

The specification can be found in the [VTK File Formats page](https://kitware.github.io/vtk-examples/site/VTKFileFormats/#hdf-file-formats).
